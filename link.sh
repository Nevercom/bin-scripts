#!/bin/bash
WEBPAGE=`curl $1 -s`
echo $WEBPAGE | grep -o '<a .*href=.*>' \
    | sed -e 's/<a /\n<a /g' \
    | sed -e 's/<a .*href=['"'"'"]//' -e 's/["'"'"'].*$//' -e '/^$/ d'\
    | sort \
    | uniq
